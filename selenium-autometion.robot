*** Settings ***
Library     String
Library     SeleniumLibrary

*** Variables ***
${Browser}      chrome
${HomePage}     automationpractice.com/index.php
${Scheme}       http
${TestURL}      ${Scheme}://${HomePage}

*** Keyword ***
Open HomePage
    Open Browser    ${TestURL}      ${Browser}

*** Test Cases ***
C001 Hacer Click en Contenedores
    Open HomePage
    Set Global Variable     @{NombresDeContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :For    ${NombreDeContenedor}   IN      @{NombresDeContenedores}
    \   Click Element       xpath=${NombreDeContenedor}
    \   Wait Until Element Is Visible   xpath=//*[@id="bigpic"]
    \   Click Element       xpath=//*[@id="header_logo"]/a/img
    Close Browser

C002 Caso de Prueba Nuevo
    Open HomePage